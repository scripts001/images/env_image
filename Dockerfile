FROM python:3-slim-bullseye

RUN apt-get update \
    && apt-get install -y --no-install-recommends gcc git bash wget zip \
    && pip --no-cache-dir install \
    ansible \
    ansible-lint \
    molecule \
    molecule-plugins[docker] \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


ADD https://download.docker.com/linux/static/stable/x86_64/docker-26.0.1.tgz ./
RUN tar xzvf docker-26.0.1.tgz --strip 1 \
    -C /usr/local/bin docker/docker \
    && rm docker-26.0.1.tgz
